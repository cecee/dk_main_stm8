

#define DEBUG 
#define MALAY 0
#define MilliSec       1
#define Sec           10
#define PW_OFF_DELAY	5

//----------------------------------------------
// GPIOB
//----------------------------------------------
#define BDIP_PORT (GPIOB)
#define CH_RESET (GPIO_PIN_3) //PB4
#define SCL  (GPIO_PIN_4) 
#define SDA  (GPIO_PIN_5) 
#define PWR_EN_SYS (GPIO_PIN_6) //PB6 PSW_PIN
#define BOUNS (GPIO_PIN_7) //PB7

//----------------------------------------------
// GPIOC
//----------------------------------------------
#define  LAMP_PC1 (GPIO_PIN_1) //PC1
#define  EX_COIN_PC2   (GPIO_PIN_2) //PC2 OUT
//#define LED1_PIN  (GPIO_PIN_4) //PC3 OUTPUT
//#define LED0_PIN  (GPIO_PIN_3) //PC4 OUTPUT
#define ONAIR_PC5  (GPIO_PIN_5) //PC5 OUTPUT//ONAIR_PC5
#define RES_RK    (GPIO_PIN_6) //PC6 OUTPUT
#define RES_SAM   (GPIO_PIN_7) //PC7 OUTPUT

//----------------------------------------------
// GPIOD
//----------------------------------------------
#define   COIN_IN  (GPIO_PIN_0) //PD0
#define   EMER_IN  (GPIO_PIN_2) //PD2
#define   EMER_OUT (GPIO_PIN_3) //PD3
#define   BZ_PD4   (GPIO_PIN_4) //PD3
#define 	BDIP_OUT (GPIO_PIN_7) //PD7

//----------------------------------------------
// GPIOE
//----------------------------------------------
#define DC_CTL_PE5     (GPIO_PIN_5) //PE5 DC_CTL
#define MUTE_RELAY  (GPIO_PIN_4) //PF4

//=============================================
#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사

//#define BDIP_PERIOD 10  //100msec  10ms*10=100ms
#define BDIP_PERIOD 100  //1000msec  10ms*100=1000ms         

typedef unsigned char			uint8;
typedef signed char				int8;
typedef unsigned short		uint16;
typedef signed short			int16;


//PWR_STATUS pPWR;
typedef struct 
{
	u8 CurONOFF;
	u8 lFlag;
	u8 PWR_MODE;
	u8 BootFail;
}PWR_STATUS;


void _printf(u8 *pFmt, u32 wVal);
void Wait_Delay(u16 data);
//u8 i2c_write_byte(u8 Addr, u8 Reg, u8 Data);
//u16 I2C_ReadByte(u8 Addr, u8 Reg);

void SOUND_REC_SEL(u8 data);
void SOUND_MUTE(u8 data);
void POWER_LED(u8 data);
void sam_init(void);
void NRPN_SND(u8 data1, u8 data2, u8 data3);
void Get_eeprom(void);
void BDipSigOut(void);
//void delay_ms(u16 delay);